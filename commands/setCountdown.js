const moment = require('moment');
require('moment-countdown');

module.exports = {
  name: '!setcountdown',
  description: 'Sets a countdown and pins it to the top of the channel',
  async execute(msg, [day, time, ...title]) {
    if (!day) {
      return msg.channel.send(`You didn't provide any arguments, ${msg.author}!`);
    }

    const countDown = moment(`${day} ${time}`, 'DD/MM/YYYY hh:mm').countdown().toString()

    const message = await msg.channel.send(`${title.join(' ')} ${countDown}`);
    await message.pin();

    setTimeout(() => editMessage(message, [day, time, ...title]), 1000)
  },
};

async function editMessage(message, [day, time, ...title]) {
  const countDown = moment(`${day} ${time}`, 'DD/MM/YYYY hh:mm').countdown().toString()
  await message.edit(`${title.join(' ')} ${countDown}`);
  setTimeout(() => editMessage(message, [day, time, ...title]), 1000);
}
