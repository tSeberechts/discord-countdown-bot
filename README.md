# Discord countdown bot

This bot creates a pinned message counting down to the provided date/time

## Usage
```
!setcountdown DD/MM/YYYY HH:mm [title]
``` 

## Example
```
!setcountdown 27/05/2020 22:33 Crew dragon launch in ...
```

## Installation
[https://discord.com/api/oauth2/authorize?client_id=715166289433198612&permissions=10240&scope=bot](https://discord.com/api/oauth2/authorize?client_id=715166289433198612&permissions=10240&scope=bot)
